package com.banking.account.security

internal class JWTUser(val uniqueId: String, val uuid: String)