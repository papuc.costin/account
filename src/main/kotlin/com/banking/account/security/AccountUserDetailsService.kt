package com.banking.account.security

import com.banking.account.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service

@Service
class AccountUserDetailsService @Autowired
constructor(private val userRepository: UserRepository) : UserDetailsService {

    override fun loadUserByUsername(username: String): UserDetails {
        val (_, uuid, uniqueId, password) = userRepository.findByUniqueId(username)
                ?: throw UsernameNotFoundException(username)
        return AccountUserDetails(uniqueId, password, uuid)
    }
}