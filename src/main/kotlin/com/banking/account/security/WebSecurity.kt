package com.banking.account.security

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpMethod
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.CorsConfigurationSource
import org.springframework.web.cors.UrlBasedCorsConfigurationSource
import org.springframework.web.filter.CorsFilter

@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
class WebSecurity @Autowired
constructor(
        private val userDetailsService: AccountUserDetailsService, 
        private val bCryptPasswordEncoder: BCryptPasswordEncoder
) : WebSecurityConfigurerAdapter() {

    @Value("\${app.security.jwt.secret}")
    private val secret: String? = null

    @Value("\${app.security.jwt.expiration-time}")
    private val expirationTime: Long? = null

    @Throws(Exception::class)
    override fun configure(http: HttpSecurity) {
        http.cors().configurationSource(corsConfigurationSource())
                .and().csrf().disable().authorizeRequests()
                .antMatchers(HttpMethod.POST, "/users").permitAll()
                .antMatchers(
                        "/",
                        "/index.html", 
                        "/favicon.ico",
                        "/manifest.json",
                        "/asset-manifest.json",
                        "/manifest.json",
                        "/robots.txt",
                        "/static/js/2.0d5c4a7d.chunk.js",
                        "/static/js/2.0d5c4a7d.chunk.js.map",
                        "/static/js/main.673cbdec.chunk.js",
                        "/static/js/main.673cbdec.chunk.js.map",
                        "/static/js/runtime-main.ed566cd0.js",
                        "/static/js/runtime-main.ed566cd0.js.map",
                        "/static/css/main.5ecd60fb.chunk.css",
                        "/static/css/main.5ecd60fb.chunk.css.map"
                ).permitAll()
                .anyRequest().authenticated()
                .and()
                .addFilter(JWTAuthenticationFilter(secret!!, expirationTime!!, authenticationManager()))
                .addFilter(JWTAuthorizationFilter(secret!!, authenticationManager()))
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
    }

    @Throws(Exception::class)
    public override fun configure(auth: AuthenticationManagerBuilder) {
        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder)
    }

    internal fun corsConfigurationSource(): CorsConfigurationSource {
        val source = UrlBasedCorsConfigurationSource()
        val config = CorsConfiguration().applyPermitDefaultValues()
        config.addExposedHeader("Authorization")
        source.registerCorsConfiguration("/**", config)
        return source
    }
}
