package com.banking.account.security

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm.HMAC512
import com.banking.account.domain.AuthUserDto
import com.banking.account.exception.BadAuthenticationRequestException
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import java.io.IOException
import java.util.*
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class JWTAuthenticationFilter : UsernamePasswordAuthenticationFilter {

    private val secret: String
    private val expirationTime: Long

    constructor(secret: String, expirationTime: Long, authenticationManager: AuthenticationManager) : super() {
        this.secret = secret
        this.expirationTime = expirationTime
        this.authenticationManager = authenticationManager
    }

    override fun attemptAuthentication(req: HttpServletRequest,
                                       res: HttpServletResponse?): Authentication {
        try {
            val (uniqueId, password) = ObjectMapper()
                    .readValue(req.inputStream, AuthUserDto::class.java)

            return authenticationManager.authenticate(
                    UsernamePasswordAuthenticationToken(
                            uniqueId,
                            password)
            )
        } catch (e: IOException) {
            throw BadAuthenticationRequestException()
        }

    }

    override fun successfulAuthentication(
            req: HttpServletRequest,
            res: HttpServletResponse,
            chain: FilterChain?,
            auth: Authentication
    ) {

        val user = auth.principal as AccountUserDetails
        val token = JWT.create()
                .withSubject(user.username)
                .withClaim("userUuid", user.uuid)
                .withExpiresAt(Date(System.currentTimeMillis() + this.expirationTime))
                .sign(HMAC512(this.secret.toByteArray()))
        res.addHeader("Authorization", "Bearer $token")
    }
}