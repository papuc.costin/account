package com.banking.account.security

import org.springframework.security.core.userdetails.User

class AccountUserDetails(username: String, password: String, val uuid: String) : User(username, password, emptyList())