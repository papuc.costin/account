package com.banking.account.mapper

interface Mapper<E, D, C> {
    fun fromEntityToDto(entity: E): D
    fun fromCreateDtoToEntity(createDto: C): E
}