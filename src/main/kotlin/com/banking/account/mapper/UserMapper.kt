package com.banking.account.mapper

import com.banking.account.domain.CreateUserDto
import com.banking.account.domain.UserDto
import com.banking.account.domain.UserEntity
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Component

@Component
class UserMapper @Autowired constructor(val passwordEncoder: BCryptPasswordEncoder)
    : Mapper<UserEntity, UserDto, CreateUserDto>
{

    override fun fromEntityToDto(entity: UserEntity): UserDto {
        return UserDto(
                uuid = entity.uuid,
                firstName = entity.firstName,
                lastName = entity.lastName,
                phoneNumber = entity.phoneNumber,
                email = entity.email,
                createdTimestamp = entity.createdTimestamp,
                updatedTimestamp = entity.updatedTimestamp
        )
    }

    override fun fromCreateDtoToEntity(createDto: CreateUserDto): UserEntity {
        return UserEntity(
            uniqueId = createDto.uniqueId,
            password = passwordEncoder.encode(createDto.password),
            firstName = createDto.firstName,
            lastName = createDto.lastName,
            phoneNumber = createDto.phoneNumber,
            email = createDto.email
        )
    }
}