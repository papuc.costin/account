package com.banking.account

import java.time.DayOfWeek
import java.time.LocalTime
import java.util.HashMap
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component
import org.springframework.validation.annotation.Validated
import java.time.ZoneId
import javax.validation.constraints.NotNull

@Component
@ConfigurationProperties(prefix = "availability")
@Validated
class AvailabilityProperties {
    var days = HashMap<DayOfWeek, TimeRange>()
    @NotNull lateinit var zoneId: ZoneId

    class TimeRange {
        @NotNull lateinit var start: LocalTime
        @NotNull lateinit var end: LocalTime

        override fun toString(): String {
            return "($start - $end)"
        }
    }

    override fun toString(): String {
        return "$days, Time Zone $zoneId"
    }
}

