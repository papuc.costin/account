package com.banking.account.domain

import com.fasterxml.jackson.annotation.JsonProperty

data class AuthUserDto(
        @JsonProperty("uniqueId") val uniqueId: String,
        @JsonProperty("password") val password: String
)