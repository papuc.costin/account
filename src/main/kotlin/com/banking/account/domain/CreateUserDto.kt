package com.banking.account.domain

import com.fasterxml.jackson.annotation.JsonProperty

data class CreateUserDto(
        @JsonProperty("uniqueId") val uniqueId: String,
        @JsonProperty("password") val password: String,
        @JsonProperty("firstName") val firstName: String,
        @JsonProperty("lastName") val lastName: String,
        @JsonProperty("phoneNumber") val phoneNumber: String,
        @JsonProperty("email") val email: String
)