package com.banking.account.domain

import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.UpdateTimestamp
import java.time.Instant
import java.util.*
import javax.persistence.*

@Entity(name="user")
@Table(
        name = "account_user",
        uniqueConstraints = [
            UniqueConstraint(columnNames = ["unique_id"])
        ]
)
data class UserEntity(

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id", nullable = false)
        val id: Long = 0,

        @Column(name = "uuid", nullable = false)
        val uuid: String = UUID.randomUUID().toString(),

        @Column(name = "unique_id", nullable = false)
        val uniqueId: String,
        
        @Column(name = "password", nullable = false)
        val password: String,

        @Column(name = "first_name", nullable = false)
        var firstName: String,

        @Column(name = "last_name", nullable = false)
        var lastName: String,

        @Column(name = "phone_number", nullable = true)
        var phoneNumber: String,

        @Column(name = "email", nullable = true)
        var email: String
) {

    @CreationTimestamp
    @Column(name = "created_timestamp", nullable = false)
    lateinit var createdTimestamp: Instant

    @UpdateTimestamp
    @Column(name = "updated_timestamp", nullable = false)
    lateinit var updatedTimestamp: Instant
}