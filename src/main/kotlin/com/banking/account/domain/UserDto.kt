package com.banking.account.domain

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonProperty
import java.time.Instant

data class UserDto(
        @JsonProperty("uuid") val uuid: String,
        @JsonProperty("firstName") val firstName: String,
        @JsonProperty("lastName") val lastName: String,
        @JsonProperty("phoneNumber") val phoneNumber: String,
        @JsonProperty("email") val email: String,

        @JsonFormat(
                shape=JsonFormat.Shape.STRING, 
                pattern="yyyy-MM-dd'T'HH:mm:ss.SSSZ", 
                timezone = "UTC"
        )
        @JsonProperty("createdTimestamp") val createdTimestamp: Instant,
        
        @JsonFormat(
                shape=JsonFormat.Shape.STRING,
                pattern="yyyy-MM-dd'T'HH:mm:ss.SSSZ",
                timezone = "UTC"
        )
        @JsonProperty("updatedTimestamp") val updatedTimestamp: Instant
)