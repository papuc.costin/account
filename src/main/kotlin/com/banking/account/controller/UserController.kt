package com.banking.account.controller

import com.banking.account.domain.CreateUserDto
import com.banking.account.domain.UserDto
import com.banking.account.exception.OperationUnavailableException
import com.banking.account.exception.UserAlreadyExistsException
import com.banking.account.mapper.UserMapper
import com.banking.account.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import org.springframework.http.HttpStatus
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.security.core.Authentication
import org.springframework.web.server.ResponseStatusException

@RestController
@RequestMapping("/users")
class UserController @Autowired constructor(
        private val service: UserService,
        private val mapper: UserMapper
) {

    @PostMapping
    fun create(@RequestBody createUserDto: CreateUserDto): UserDto {
        try {
            val userEntity = mapper.fromCreateDtoToEntity(createUserDto)
            val userEntityCreated = service.createUser(userEntity)
            val userDto = mapper.fromEntityToDto(userEntityCreated)
            return userDto
        } catch (exc: OperationUnavailableException) {
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, exc.message)
        } catch (exc: UserAlreadyExistsException) {
            throw ResponseStatusException(HttpStatus.CONFLICT, exc.message)
        }
    }

    @GetMapping("/{uuid}")
    @PreAuthorize("#uuid == authentication.principal.uuid")
    fun get(@PathVariable("uuid") uuid: String, authentication: Authentication): UserDto {
        val userEntity = service.getUser(uuid)
        return mapper.fromEntityToDto(userEntity)
    }

}