package com.banking.account.util

import org.postgresql.util.PSQLException

object SqlUtil {

    fun isConstraintViolationException(e: Throwable): Boolean {
        var t: Throwable? = e
        while (t != null) {
            if (t is PSQLException) {
                //in Postgres SQL state 23505 = unique_violation
                return "23505".equals(t.sqlState)
            }
            t = t.cause
        }
        return false
    }
}