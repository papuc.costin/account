package com.banking.account.service

import com.banking.account.AvailabilityProperties
import com.banking.account.AvailabilityProperties.TimeRange
import com.banking.account.exception.OperationUnavailableException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.time.ZonedDateTime
import java.time.ZoneId
import java.time.Instant



@Service
class AvailabilityService @Autowired constructor(
        private val availabilityProps: AvailabilityProperties
) {

    fun check() {

        val now: ZonedDateTime = getCurrentTime(availabilityProps.zoneId)
        val timeRange: TimeRange = availabilityProps.days.get(now.dayOfWeek)
                ?: throw OperationUnavailableException(
                        "Operation unavailable outside working days. Availability " + availabilityProps
                )

        if (now.toLocalTime().isAfter(timeRange.start)
                && now.toLocalTime().isBefore(timeRange.end)) {

            return
        }
        else {
            throw OperationUnavailableException(
                    "Operation unavailable outside working hours. Availability " + availabilityProps
            )
        }
    }

    fun getCurrentTime(zoneId: ZoneId): ZonedDateTime {
        return ZonedDateTime.now(zoneId)
    }
}