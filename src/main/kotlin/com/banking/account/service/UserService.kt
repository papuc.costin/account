package com.banking.account.service

import com.banking.account.domain.UserEntity
import com.banking.account.exception.UserAlreadyExistsException
import com.banking.account.repository.UserRepository
import com.banking.account.util.SqlUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class UserService @Autowired constructor(
        private val repository: UserRepository,
        private val availabilityService: AvailabilityService
) {

    @Transactional
    fun createUser(userEntity: UserEntity): UserEntity {
        availabilityService.check()

        try {
            return repository.saveAndFlush(userEntity)
        } catch (exc: DataIntegrityViolationException) {
            if (SqlUtil.isConstraintViolationException(exc)) {
                throw UserAlreadyExistsException()
            } else {
                throw exc
            }
        }
    }

    fun getUser(uuid: String): UserEntity {
        return repository.findByUuid(uuid)
    }
}