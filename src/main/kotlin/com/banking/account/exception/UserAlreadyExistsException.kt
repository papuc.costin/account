package com.banking.account.exception

import java.lang.RuntimeException

class UserAlreadyExistsException : RuntimeException {
    constructor() : super("User with provided unique id exists", null, false, false)
}