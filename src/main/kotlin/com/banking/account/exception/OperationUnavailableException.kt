package com.banking.account.exception

import java.lang.RuntimeException

class OperationUnavailableException : RuntimeException {
    constructor(message: String?) : super(message, null, false, false)
}