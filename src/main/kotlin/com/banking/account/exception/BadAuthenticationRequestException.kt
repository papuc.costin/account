package com.banking.account.exception

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus
import java.lang.RuntimeException

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
class BadAuthenticationRequestException : RuntimeException {
    constructor() : super("Bad authentication request body", null, false, false)
}