package com.banking.account.repository

import com.banking.account.domain.UserEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface UserRepository : JpaRepository<UserEntity, Long> {
    fun findByUniqueId(uniqueId: String): UserEntity?
    fun findByUuid(uuid: String): UserEntity
}
