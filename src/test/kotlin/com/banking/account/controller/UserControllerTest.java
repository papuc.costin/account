package com.banking.account.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.banking.account.domain.CreateUserDto;
import com.banking.account.domain.UserDto;
import com.banking.account.exception.OperationUnavailableException;
import com.banking.account.exception.UserAlreadyExistsException;
import com.banking.account.mapper.UserMapper;
import com.banking.account.security.AccountUserDetailsService;
import com.banking.account.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.time.Instant;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@ExtendWith(SpringExtension.class)
@WebMvcTest(UserController.class)
@TestInstance(Lifecycle.PER_CLASS)
class UserControllerTest {
	
	@MockBean
	private UserService service;

	@MockBean
	private UserMapper mapper;
	
	@MockBean
	private AccountUserDetailsService accountUserDetailsService;
	
	@Autowired
	private WebApplicationContext context;

	private MockMvc mockMvc;

	@BeforeAll
	public void setup() {
		mockMvc = MockMvcBuilders
				.webAppContextSetup(context)
				.apply(springSecurity())
				.build();
	}

	private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
	private static final CreateUserDto REQ = new CreateUserDto(
			"1234", "password", "Ion", "Marin", "08989089", "email@email.com"
	);

	@WithMockUser("spring")
	@Test
	public void createUserTestOk() throws Exception {

		UserDto userDto = new UserDto("uuid_test", REQ.getFirstName(),
				REQ.getLastName(), REQ.getPhoneNumber(), REQ.getEmail(), Instant.now(), Instant.now());

		Mockito.doReturn(userDto).when(mapper).fromEntityToDto(any());

		mockMvc.perform(post("/users")
						.content(OBJECT_MAPPER.writeValueAsString(REQ))
						.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.uuid").value("uuid_test"))
				.andExpect(jsonPath("$.firstName").value("Ion"))
				.andExpect(jsonPath("$.lastName").value("Marin"))
				.andExpect(jsonPath("$.phoneNumber").value("08989089"))
				.andExpect(jsonPath("$.email").value("email@email.com"));

		Mockito.verify(mapper).fromCreateDtoToEntity(any());
		Mockito.verify(service).createUser(any());
	}	
	
	@WithMockUser("spring")
	@Test
	public void createUserAlreadyExists() throws Exception {

		Mockito.doThrow(UserAlreadyExistsException.class).when(service).createUser(any());

		mockMvc.perform(post("/users")
						.content(OBJECT_MAPPER.writeValueAsString(REQ))
						.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isConflict());
	}

	@WithMockUser("spring")
	@Test
	public void createUnavailableService() throws Exception {

		Mockito.doThrow(OperationUnavailableException.class).when(service).createUser(any());

		mockMvc.perform(post("/users")
						.content(OBJECT_MAPPER.writeValueAsString(REQ))
						.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest());
	}
}