package com.banking.account.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.banking.account.AvailabilityProperties;
import com.banking.account.AvailabilityProperties.TimeRange;
import com.banking.account.exception.OperationUnavailableException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.HashMap;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

public class AvailabilityServiceTest {
	
	private static final ZoneId ZONE_ID = ZoneId.of("Europe/Bucharest");
	private static final AvailabilityProperties PROPS = getProps();
	private static final AvailabilityService service = Mockito.spy(new AvailabilityService(PROPS));

	@Test
	public void serviceIsAvailable() {

		ZonedDateTime zonedDateTime = ZonedDateTime.of(
				LocalDate.of(2020, 4, 27), LocalTime.of(10, 0), ZONE_ID
		);
		Mockito.doReturn(zonedDateTime).when(service).getCurrentTime(ZONE_ID);
		
		service.check();
	}

	@Test
	public void serviceIsNotAvailableBecauseOfDay() {

		ZonedDateTime zonedDateTime = ZonedDateTime.of(
				LocalDate.of(2020, 4, 26), LocalTime.of(10, 0), ZONE_ID
		);
		Mockito.doReturn(zonedDateTime).when(service).getCurrentTime(ZONE_ID);
		
		Throwable exception = assertThrows(OperationUnavailableException.class, service::check);
		assertEquals(
				"Operation unavailable outside working days. Availability " + PROPS,
				exception.getMessage()
		);
	}

	@Test
	public void serviceIsNotAvailableBecauseOfTime() {

		ZonedDateTime zonedDateTime = ZonedDateTime.of(
				LocalDate.of(2020, 4, 27), LocalTime.of(18, 20), ZONE_ID
		);
		Mockito.doReturn(zonedDateTime).when(service).getCurrentTime(ZONE_ID);
		
		Throwable exception = assertThrows(OperationUnavailableException.class, service::check);
		assertEquals(
				"Operation unavailable outside working hours. Availability " + PROPS, 
				exception.getMessage()
		);
	}

	public static AvailabilityProperties getProps() {
		AvailabilityProperties props = new AvailabilityProperties();
		TimeRange tr = new TimeRange();
		tr.setStart(LocalTime.of(9, 0));
		tr.setEnd(LocalTime.of(18, 0));

		HashMap<DayOfWeek, TimeRange> days = new HashMap<>();
		days.put(DayOfWeek.MONDAY, tr);
		props.setDays(days);
		props.setZoneId(ZONE_ID);

		return props;
	}
}